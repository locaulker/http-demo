import React from "react"

const Todo = props => {
  const { todo } = props

  return (
    <div className="todos">
      <h3>{todo.id}</h3>
      <hr />
      <h3>{todo.title}</h3>
    </div>
  )
}

export default Todo
